// soal 1

// buatlah variabel seperti di bawah ini

var nilai = 40;
// pilih angka dari 0 sampai 100, misal 75. lalu isi variabel tersebut dengan angka tersebut. lalu buat lah pengkondisian dengan if-elseif dengan kondisi

// nilai >= 85 indeksnya A
// nilai >= 75 dan nilai < 85 indeksnya B
// nilai >= 65 dan nilai < 75 indeksnya c
// nilai >= 55 dan nilai < 65 indeksnya D
// nilai < 55 indeksnya E

if (nilai >= 85) {
  console.log("Nilai A");
} else if (nilai >= 75 && nilai < 85) {
  console.log("Nilai B");
} else if (nilai >= 65 && nilai < 75) {
  console.log("Nilai C");
} else if (nilai >= 55 && nilai < 65) {
  console.log("Nilai D");
} else {
  console.log("Nilai E");
}

// soal 2

// buatlah variabel seperti di bawah ini

var tanggal = 20;
var bulan = 12;
var tahun = 1999;
// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing)

switch (bulan) {
  case 1: {
    bulan = "Januari";
    break;
  }
  case 2: {
    bulan = "Februari";
    break;
  }
  case 3: {
    bulan = "Maret";
    break;
  }
  case 4: {
    bulan = "April";
    break;
  }
  case 5: {
    bulan = "Mei";
    break;
  }
  case 6: {
    bulan = "Juni";
    break;
  }
  case 7: {
    bulan = "Juli";
    break;
  }
  case 8: {
    bulan = "Agustus";
    break;
  }
  case 9: {
    bulan = "September";
    break;
  }
  case 10: {
    bulan = "Oktober";
    break;
  }
  case 11: {
    bulan = "November";
    break;
  }
  case 12: {
    bulan = "Desember";
    break;
  }
  default: {
    console.log("Bulan Tidak Ditemukan");
  }
}
console.log(tanggal, bulan, tahun);

// soal 3
// Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi n dan alas n. Looping boleh menggunakan syntax apa pun (while, for, do while).

// Output untuk n=3 :

// #
// ##
// ###
// Output untuk n=7 :

// #
// ##
// ###
// ####
// #####
// ######
// #######

var n = 7;
var hasil = "";
for (var i = 0; i < n; i++) {
  for (var j = 0; j <= i; j++) {
    hasil += "#";
  }
  hasil += "\n";
}
console.log(hasil);

// soal 4
// berilah suatu nilai m dengan tipe integer, dan buatlah pengulangan dari 1 sampai dengan m, dan berikan output sebagai berikut.
// contoh :

// Output untuk m = 3

// 1 - I love programming
// 2 - I love Javascript
// 3 - I love VueJS
// ===

var m = 10;
var a = "";
for (var i = 1; i <= m; i++) {
  console.log(i + " - I Love Programming");
  if (i != m) {
    for (var j = (i += 1); j <= i; j++) {
      console.log(j + " - I Love Javascript");
      for (var k = (j += 1); k <= j; k++) {
        console.log(k + " - I Love VueJs");
        i = k;
        a += "===";
        console.log(a);
      }
    }
  }
}
