// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
// Jawaban Soal 1
var aPertama = pertama.substr(0, 5);
var bPertama = pertama.substr(12, 6);
var aKedua = kedua.substr(0, 8);
var bKedua = kedua.substr(8, 10);
var cKedua = bKedua.toUpperCase();

var akhir = aPertama.concat(bPertama) + " " + aKedua.concat(cKedua);

console.log(akhir);

// Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
// Jawaban soal 2
var a = parseInt(kataPertama);
var b = parseInt(kataKedua);
var c = parseInt(kataKetiga);
var d = parseInt(kataKeempat);

var hasil = d * c + (a % b);
console.log(hasil);

// Soal 3
var kalimat = "wah javascript itu keren sekali";
// Jawaban Soal 3
var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
