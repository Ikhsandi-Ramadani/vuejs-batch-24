var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

// Lanjutkan code untuk menjalankan function readBooksPromise
const baca = (time, books, i) => {
  if (i < books.length) {
    // Belum dapet logicnya
    readBooksPromise(time, books[i])
      .then((sisa) => {
        if (sisa > 0) {
          i += 1;
          baca(sisa, books, i);
        }
      })
      .catch(() => {
        baca(sisa, books, i);
      });
  }
};

baca(5000, books, 0);
