let vm = new Vue({
  el: "#app",
  data: {
    users: [
      {
        name: "Muhammad Iqbal Mubarok",
      },
      {
        name: "Ruby Purwati",
      },
      {
        name: "Faqih Muhammad",
      },
    ],
  },
  methods: {
    name: '',
    tambah: function () {
      this.users.push({ name: this.users.name });
    },
    edit: function (name) {
      this.users.name = this.users[name];
      console.log(this.editInput);
      this.users.splice(name, 1);
    },
    update: function () {
      this.users.push({ name: this.users.name });
    },
    delete: function (name) {
      this.users.splice(name, 1);
    },
  },
});
