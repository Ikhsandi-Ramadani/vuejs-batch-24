// Judul : Function Penghasil Tanggal Hari Esok

// Buatlah sebuah function dengan nama next_date() yang menerima 3 parameter tanggal, bulan, tahun dan mengembalikan nilai tanggal hari esok dalam bentuk string, dengan contoh input dan otput sebagai berikut.
function next_date() {
  var d = new Date();
  var besok = new Date(d);
  besok.setDate(besok.getDate() + 1);
  return besok;
}
// Belum Dapet Logicnya
var tanggal = 28;
var bulan = 2;
var tahun = 2021;

console.log(next_date());

// Judul : Function Penghitung Jumlah Kata

// Buatlah sebuah function dengan nama jumlah_kata() yang menerima sebuah kalimat (string), dan mengembalikan nilai jumlah kata dalam kalimat tersebut.

// Contoh
function jumlah_kata(kata) {
  var hps = kata.trim();
  banyak_kata = hps.split(" ");
  return banyak_kata.length;
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok ";
var kalimat_2 = "Saya Iqbal";

var k1 = jumlah_kata(kalimat_1); // 6
var k2 = jumlah_kata(kalimat_2); // 2

console.log(k1);
console.log(k2);
